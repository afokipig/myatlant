// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/TargetTrackedWidgetComponent.h"


void UTargetTrackedWidgetComponent::BeginPlay()
{
	Super::BeginPlay();
	SetTarget(nullptr);
}

void UTargetTrackedWidgetComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	//if (Target == nullptr) return;

	//FVector TargetDirection(GetComponentLocation() - Target->GetActorLocation());
	//TargetDirection.Normalize();

	//FTransform MyTransform{ GetComponentTransform() };
	//MyTransform.SetRotation(FQuat(TargetDirection.Rotation()));
	//SetComponentToWorld(MyTransform);
}

void UTargetTrackedWidgetComponent::SetTarget(AActor* InTarget)
{
	if (Target)
		Target->OnDestroyed.RemoveDynamic(this, &UTargetTrackedWidgetComponent::PopTarget);

	Target = InTarget;

	if (Target)
	{
		Target->OnDestroyed.AddUniqueDynamic(this, &UTargetTrackedWidgetComponent::PopTarget);
		SetVisibility(true, false);
	}
	else
		SetVisibility(false, false);
}

void UTargetTrackedWidgetComponent::PopTarget(AActor* InTarget)
{
	if (Target == InTarget)
		SetTarget(nullptr);
}

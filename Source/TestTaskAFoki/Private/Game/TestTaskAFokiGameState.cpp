// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/TestTaskAFokiGameState.h"
#include "Game/TestTaskAFokiGameMode.h"
#include "Game/CollectablesSpawner.h"
#include "Interactive/VictoryPart.h"
#include "Net/UnrealNetwork.h"
#include "Kismet/GameplayStatics.h"
#include "EngineUtils.h"


namespace
{
	const static FString DefaultVictoryPartName{ TEXT("Part") };
}

void ATestTaskAFokiGameState::Tick(float InDeltaTime)
{
	Super::Tick(InDeltaTime);

	if (GameStatus == EGameStatus::InGame)
	{
		TimeToEndGame -= InDeltaTime;
		if (HasAuthority() && TimeToEndGame <= 0.f)
			EndGame(VictoryPartCollected >= PartsCountToVictory);
	}
}

void ATestTaskAFokiGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATestTaskAFokiGameState, VictoryPartType);
	DOREPLIFETIME(ATestTaskAFokiGameState, VictoryPartCollected);
	DOREPLIFETIME(ATestTaskAFokiGameState, GameStatus);
	DOREPLIFETIME(ATestTaskAFokiGameState, TimeToEndGame);
}

void ATestTaskAFokiGameState::BeginPlay()
{
	Super::BeginPlay();

	for (TActorIterator<ACollectablesSpawner> It(GetWorld()); It; ++It)
	{
		if (It->HasAnyFlags(RF_ClassDefaultObject)) continue;

		Spawners.Add(*It);
	}
}

void ATestTaskAFokiGameState::SetVictoryPartType(TSubclassOf<AVictoryPart> InType)
{
	if (InType == nullptr) return;
	if (VictoryPartType != nullptr)
	{
		checkf(VictoryPartType == nullptr, TEXT("ATestTaskAFokiGameState::VictoryPartType can be set only once"));
		return;
	}

	VictoryPartType = InType;
	OnVictoryPartTypeChanged(GetVictoryPartName(InType));
}

void ATestTaskAFokiGameState::SetGameStatus(EGameStatus InGameStatus)
{
	GameStatus = InGameStatus;
	OnGameStatusChanged.Broadcast(InGameStatus);

	if (InGameStatus == EGameStatus::InGame)
		StartGame();
}

void ATestTaskAFokiGameState::UpdateGamePartsInWorld(const TSet<TSubclassOf<AVictoryPart>> PartsTypes, bool bDeleteVictoryParts)
{
	TMap<int32, UClass*> NextVictoryPartSetup;
	for (int32 RandIndex; UClass * PartClass : PartsTypes)
	{
		RandIndex = FMath::RandRange(0, Spawners.Num() - 1);
		if (UClass * *MappedClass{ NextVictoryPartSetup.Find(RandIndex) })
		{
			if (*MappedClass != VictoryPartType)
				*MappedClass = PartClass;
		}
		else
		{
			NextVictoryPartSetup.Add(RandIndex, PartClass);
		}
	}

	ACollectablesSpawner* SpawnerIt;
	for (int32 I{ 0 }; I < Spawners.Num(); ++I)
	{
		SpawnerIt = Spawners[I];

		if (UClass * *MappedClass{ NextVictoryPartSetup.Find(I) })
		{
			if (SpawnerIt->GetSpawnedObject()
				&& (!SpawnerIt->GetSpawnedObject()->IsA(VictoryPartType) || bDeleteVictoryParts))
				SpawnerIt->GetSpawnedObject()->Destroy();

			SpawnerIt->SpawnActorOfClass(*MappedClass);
		}
	}
}

bool ATestTaskAFokiGameState::TryCollectVictoryPart(AVictoryPart* InPart, AController* InCollector)
{
	if (InPart->GetClass() != VictoryPartType
		|| !InPart->IsValidPart()
		|| InCollector->GetPawn() == nullptr)
		return false;

	InPart->SetCollected();
	OnVictoryPartCollected(InPart, ++VictoryPartCollected);

	if (VictoryPartCollected >= PartsCountToVictory)
		EndGame(true);
	return true;
}

void ATestTaskAFokiGameState::BroadcastVictoryPartInfo()
{
	VictoryPartsCountChanged.Broadcast(VictoryPartCollected, PartsCountToVictory);
	VictoryPartTypeChanged.Broadcast(GetVictoryPartName(VictoryPartType));
}

const FString& ATestTaskAFokiGameState::GetVictoryPartName() const
{
	return GetVictoryPartName(VictoryPartType);
}

const FString& ATestTaskAFokiGameState::GetVictoryPartName(const UClass* const InType) const
{
	if (InType)
	{
		AVictoryPart* DefaultVictoryPart{ Cast<AVictoryPart>(InType->GetDefaultObject()) };
		return DefaultVictoryPart != nullptr ? DefaultVictoryPart->GetPartName() : DefaultVictoryPartName;
	}
	return DefaultVictoryPartName;
}

void ATestTaskAFokiGameState::OnVictoryPartCollected_Implementation(AVictoryPart* InPart, int32 InCurrentPartsCount)
{
	if (InPart)
	{
		InPart->Destroy();
		OnVictoryPartDestroyed.ExecuteIfBound();
	}
	VictoryPartsCountChanged.Broadcast(InCurrentPartsCount, PartsCountToVictory);
}

void ATestTaskAFokiGameState::OnVictoryPartTypeChanged_Implementation(const FString& InTypeName)
{
	VictoryPartTypeChanged.Broadcast(InTypeName);
}

void ATestTaskAFokiGameState::StartGame()
{
	TimeToEndGame = GameTime;
	OnVictoryPartCollected(nullptr, VictoryPartCollected);
}

void ATestTaskAFokiGameState::EndGame(bool bVictory)
{
	if (!HasAuthority()) return;
	GameResult = bVictory ? EGameResult::Win : EGameResult::Lose;
	SetGameStatus(EGameStatus::GameOver);
}

void ATestTaskAFokiGameState::OnRep_GameStatus()
{
	OnGameStatusChanged.Broadcast(GameStatus);
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/CollectablesSpawner.h"
#include "Components/ArrowComponent.h"
#include "Components/StaticMeshComponent.h"


// Sets default values
ACollectablesSpawner::ACollectablesSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpawnerMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SpawnerMesh"));
	RootComponent = SpawnerMesh;

	SpawnArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("SpawnArrow"));
	SpawnArrow->SetupAttachment(SpawnerMesh);
}

void ACollectablesSpawner::Tick(float InDeltaTime)
{
	Super::Tick(InDeltaTime);

	float DeltaRotation{ SpawnerMeshRotationSpeed * InDeltaTime };

	FVector MeshRotation{ SpawnerMesh->GetComponentRotation().Vector() };
	MeshRotation = MeshRotation.RotateAngleAxis(DeltaRotation, FVector::ZAxisVector);
	SpawnerMesh->SetWorldRotation(MeshRotation.Rotation());
}

void ACollectablesSpawner::SpawnActorOfClass(UClass* InClass)
{
	if (!CanSpawn()) return;

	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnedObject = GetWorld()->SpawnActor(InClass, &SpawnArrow->GetComponentTransform(), SpawnParams);
	SpawnedObject->OnDestroyed.AddUniqueDynamic(this, &ACollectablesSpawner::OnActorDestroyed);
}

void ACollectablesSpawner::OnActorDestroyed(AActor* InOther)
{
	if (SpawnedObject == InOther)
		SpawnedObject = nullptr;
}

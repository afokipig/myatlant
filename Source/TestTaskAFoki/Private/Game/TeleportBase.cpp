// Fill out your copyright notice in the Description page of Project Settings.


#include "Game/TeleportBase.h"

ATeleportBase::ATeleportBase()
{
}

bool ATeleportBase::InteractionPossible() const
{
	return Super::InteractionPossible() && Pair != nullptr;
}

bool ATeleportBase::TryInteract(AActor* InInteractor)
{
	if (!InteractionPossible() || !HasAuthority()) return false;

	Teleport(InInteractor, Pair->GetActorLocation() + FVector(0.f, 0.f, 50.f));

	return true;
}

void ATeleportBase::Teleport_Implementation(AActor* InActor, const FVector& Position)
{
	if(InActor)
		InActor->SetActorLocation(Position);
}

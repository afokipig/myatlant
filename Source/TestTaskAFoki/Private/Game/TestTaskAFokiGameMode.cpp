// Copyright Epic Games, Inc. All Rights Reserved.

#include "Game/TestTaskAFokiGameMode.h"
#include "Game/TestTaskAFokiGameState.h"
#include "UObject/ConstructorHelpers.h"
#include "Player/TestTaskAFokiPlayerController.h"


ATestTaskAFokiGameMode::ATestTaskAFokiGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void ATestTaskAFokiGameMode::PreInitializeComponents()
{
	Super::PreInitializeComponents();
	if (ATestTaskAFokiGameState * GS{ GetGameState<ATestTaskAFokiGameState>() })
	{
		GS->OnVictoryPartDestroyed.BindUObject(this, &ATestTaskAFokiGameMode::OnVictoryPartDestroyed);
	}
}

void ATestTaskAFokiGameMode::RestartPlayer(AController* NewPlayer)
{
	Super::RestartPlayer(NewPlayer);
}

void ATestTaskAFokiGameMode::StartGame()
{
	if (GameStatus != EGameStatus::InLobby) return;
	GameStatus = EGameStatus::InGame;

	if (ATestTaskAFokiGameState * GS{ GetGameState<ATestTaskAFokiGameState>() })
	{
		GS->SetVictoryPartType(GetRandomVictoryPartType());
		GS->SetGameStatus(EGameStatus::InGame);
		for (int32 I{ 0 }; I < StartVictoryPartBatches; ++I)
			GS->UpdateGamePartsInWorld(PossibleVictoryParts, false);
	}
}

void ATestTaskAFokiGameMode::OnPostLogin(AController* NewPlayer)
{
	if (ATestTaskAFokiPlayerController * PC{ Cast<ATestTaskAFokiPlayerController>(NewPlayer) })
		PC->OnPostLogin();
}

void ATestTaskAFokiGameMode::OnVictoryPartDestroyed()
{
	if (ATestTaskAFokiGameState * GS{ GetGameState<ATestTaskAFokiGameState>() })
		GS->UpdateGamePartsInWorld(PossibleVictoryParts, false);
}

TSubclassOf<class AVictoryPart> ATestTaskAFokiGameMode::GetRandomVictoryPartType() const
{
	if(PossibleVictoryParts.IsEmpty())return nullptr;

	int32 PartsTypesNum{ PossibleVictoryParts.Num() };

	int32 RandomNum{ FMath::RandRange(0, PartsTypesNum - 1) };

	for (int32 It{ 0 }; TSubclassOf<class AVictoryPart> PartType : PossibleVictoryParts)
	{
		if (It++ == RandomNum)
			return PartType;
	}

	return nullptr;
}

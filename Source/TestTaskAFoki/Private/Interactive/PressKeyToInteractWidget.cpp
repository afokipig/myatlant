// Fill out your copyright notice in the Description page of Project Settings.


#include "Interactive/PressKeyToInteractWidget.h"
#include "Components/TextBlock.h"


void UPressKeyToInteractWidget::SetKeyString(const FString& InKey)
{
	KeyText->SetText(FText::FromString(InKey));
}

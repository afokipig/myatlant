// Fill out your copyright notice in the Description page of Project Settings.


#include "Interactive/InteractiveActor.h"
#include "Interactive/InteractingPawn.h"
#include "Components/ShapeComponent.h"
#include "UI/TargetTrackedWidgetComponent.h"
#include "Interactive/PressKeyToInteractWidget.h"


// Sets default values
AInteractiveActor::AInteractiveActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	WidgetComponent = CreateDefaultSubobject<UTargetTrackedWidgetComponent>(TEXT("InteractWidget"));
	RootComponent = WidgetComponent;
}

// Called when the game starts or when spawned
void AInteractiveActor::BeginPlay()
{
	Super::BeginPlay();
	
	BindCollision(GetInteractionShape());
}

// Called every frame
void AInteractiveActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool AInteractiveActor::InteractionPossible() const
{
	return true;
}

bool AInteractiveActor::TryInteract(AActor* InInteractor)
{
	if (!InteractionPossible()) return false;

	return true;
}

void AInteractiveActor::OnInteractionShapeBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AddInteractor(OtherActor);
}

void AInteractiveActor::OnInteractionShapeEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	RemoveInteractor(OtherActor);
}

void AInteractiveActor::AddInteractor(AActor* InActor)
{
	TScriptInterface<IInteractingPawn> Interactor{ InActor };
	if (!Interactor) return;

	Interactor->PushInteractiveActor(this);
	if (!Interactor->NeedWidget()) return;

	UPressKeyToInteractWidget* Widget{ Cast<UPressKeyToInteractWidget>(WidgetComponent->GetUserWidgetObject()) };
	if (Widget == nullptr) return;

	Widget->SetKeyString(Interactor->GetInteractionKey());
	WidgetComponent->SetTarget(InActor);
}

void AInteractiveActor::RemoveInteractor(AActor* InActor)
{
	TScriptInterface<IInteractingPawn> Interactor{ InActor };
	if (!Interactor) return;

	Interactor->PopInteractiveActor(this);
	WidgetComponent->PopTarget(InActor);
}

void AInteractiveActor::RemoveAllInteractors()
{
	UShapeComponent* InteractionShape{ GetInteractionShape() };
	if (InteractionShape)
	{
		TSet<AActor*>Overlappers;
		InteractionShape->GetOverlappingActors(Overlappers);
		for (AActor* Overlapper : Overlappers)
			RemoveInteractor(Overlapper);
	}
}

void AInteractiveActor::BindCollision(UShapeComponent* InShapeComponent)
{
	if (InShapeComponent == nullptr) return;

	InShapeComponent->OnComponentBeginOverlap.AddUniqueDynamic(this, &AInteractiveActor::OnInteractionShapeBeginOverlap);
	InShapeComponent->OnComponentEndOverlap.AddUniqueDynamic(this, &AInteractiveActor::OnInteractionShapeEndOverlap);
}

UShapeComponent* AInteractiveActor::GetInteractionShape_Implementation() const
{
	return nullptr;
}


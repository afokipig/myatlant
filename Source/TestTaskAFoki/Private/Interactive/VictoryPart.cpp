// Fill out your copyright notice in the Description page of Project Settings.


#include "Interactive/VictoryPart.h"
#include "Game/TestTaskAFokiGameState.h"


AVictoryPart::AVictoryPart()
{
}

bool AVictoryPart::InteractionPossible() const
{
	ATestTaskAFokiGameState* GS{ GetWorld()->GetGameState<ATestTaskAFokiGameState>() };
	return Super::InteractionPossible()
		&& bIsValidPart
		&& GS
		&& GS->GetVictoryPartType() == GetClass();
}

bool AVictoryPart::TryInteract(AActor* InInteractor)
{
	if (!InteractionPossible()
		|| !HasAuthority()) return false;

	ATestTaskAFokiGameState* GS{ GetWorld()->GetGameState<ATestTaskAFokiGameState>() };
	if (GS == nullptr) return false;

	AController* InteractorController{ Cast<AController>(InInteractor) };
	if (InteractorController == nullptr)
	{
		APawn* InteractorPawn{ Cast<APawn>(InInteractor) };
		if (InteractorPawn == nullptr) return false;

		InteractorController = InteractorPawn->GetController();
		if (InteractorController == nullptr) return false;
	}

	return GS->TryCollectVictoryPart(this, InteractorController);
}

void AVictoryPart::Destroyed()
{
	Super::Destroyed();
	bIsValidPart = false;
}
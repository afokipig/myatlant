// Fill out your copyright notice in the Description page of Project Settings.


#include "Interactive/InteractingPawn.h"


FString IInteractingPawn::GetInteractionKey() const
{
	return FString();
}

void IInteractingPawn::PushInteractiveActor(AInteractiveActor* InActor)
{
}

void IInteractingPawn::PopInteractiveActor(AInteractiveActor* InActor)
{
}

bool IInteractingPawn::NeedWidget() const
{
	return false;
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "Interactive/DoorBase.h"
#include "Net/UnrealNetwork.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ShapeComponent.h"


ADoorBase::ADoorBase()
{
	PrimaryActorTick.bCanEverTick = true;

	DoorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("DoorMesh"));
	DoorMesh->SetupAttachment(GetRootComponent());
}

void ADoorBase::BeginPlay()
{
	Super::BeginPlay();
	BeginRelativeLocation = DoorMesh->GetRelativeLocation();
	CurrentState = CurrentDoorStatus == EDoorStatus::Open ? OpenState : CloseState;
}

void ADoorBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ADoorBase, TargetDoorStatus);
}

void ADoorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (CurrentDoorStatus != EDoorStatus::Move) return;

	CurrentState = FMath::FInterpConstantTo(CurrentState, TargetDoorState, DeltaTime, DoorMoveSpeed);
	DoorMesh->SetRelativeLocation(BeginRelativeLocation + FVector(0.f, 0.f, CurrentState));

	if (FMath::IsNearlyEqual(CurrentState, TargetDoorState))
	{
		CurrentDoorStatus = TargetDoorStatus;
		UShapeComponent* InteractionShape{ GetInteractionShape() };
		if (InteractionShape)
		{
			TSet<AActor*>Overlappers;
			InteractionShape->GetOverlappingActors(Overlappers);
			for (AActor* Overlapper : Overlappers)
				AddInteractor(Overlapper);
		}
	}
}

bool ADoorBase::InteractionPossible() const
{
	return Super::InteractionPossible() && CurrentDoorStatus != EDoorStatus::Move;
}

bool ADoorBase::TryInteract(AActor* InInteractor)
{
	if (!InteractionPossible()) return false;

	TargetDoorStatus = CurrentDoorStatus == EDoorStatus::Open ? EDoorStatus::Closed : EDoorStatus::Open;
	SetTargetDoorStatus(TargetDoorStatus);

	RemoveInteractor(InInteractor);

	return true;
}

void ADoorBase::OnRep_TargetDoorStatus()
{
	SetTargetDoorStatus(TargetDoorStatus);
}

void ADoorBase::SetTargetDoorStatus(EDoorStatus InStatus)
{
	if (TargetDoorStatus == EDoorStatus::Open)
		TargetDoorState = OpenState;
	else
		TargetDoorState = CloseState;

	CurrentDoorStatus = EDoorStatus::Move;
	RemoveAllInteractors();
}

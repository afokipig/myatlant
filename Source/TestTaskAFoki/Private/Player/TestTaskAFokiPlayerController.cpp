// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/TestTaskAFokiPlayerController.h"
#include "Player/TestTaskAFokiHUD.h"
#include "Game/TestTaskAFokiGameState.h"
#include "EnhancedInputComponent.h"


void ATestTaskAFokiPlayerController::OnPostLogin()
{
	ATestTaskAFokiGameState* GS{ GetWorld()->GetGameState<ATestTaskAFokiGameState>() };
	if (GS)
	{
		GS->OnGameStatusChanged.AddUniqueDynamic(this, &ATestTaskAFokiPlayerController::OnGameStatusChanged);
		OnGameStatusChanged(GS->CurrentGameStatus());
	}
}

void ATestTaskAFokiPlayerController::EndPlay(EEndPlayReason::Type InReason)
{
	Super::EndPlay(InReason);

	GetWorldTimerManager().ClearTimer(EndGameTimer);
}

void ATestTaskAFokiPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	UEnhancedInputComponent* EnhancedInput{ Cast< UEnhancedInputComponent>(InputComponent) };
	if (EnhancedInput)
	{
		EnhancedInput->BindAction(ShowGameInfoAction, ETriggerEvent::Started, this, &ATestTaskAFokiPlayerController::ShowGameInfo);
	}
}

void ATestTaskAFokiPlayerController::EndSession()
{
	ATestTaskAFokiHUD* HudPtr{ GetHUD<ATestTaskAFokiHUD>() };
	if (HudPtr)
	{
		HudPtr->ExitButtonClicked();
	}
}

void ATestTaskAFokiPlayerController::OnGameStatusChanged(EGameStatus InStatus)
{
	if (InStatus == EGameStatus::GameOver)
	{
		UnPossess();

		ATestTaskAFokiGameState* GS{ GetWorld()->GetGameState<ATestTaskAFokiGameState>() };
		if (GS)
			ClientGameComplete(GS->GetGameResult());
	}

	ClientGameStatusChanged(InStatus);
}

void ATestTaskAFokiPlayerController::OnHUDCreated()
{
	ATestTaskAFokiHUD* HudPtr{ GetHUD<ATestTaskAFokiHUD>() };
	if (HudPtr)
	{
		ATestTaskAFokiGameState* GS{ GetWorld()->GetGameState<ATestTaskAFokiGameState>() };
		if (GS)
		{
			ClientGameStatusChanged(GS->CurrentGameStatus());
			GS->BroadcastVictoryPartInfo();
		}
	}
}

void ATestTaskAFokiPlayerController::ClientGameStatusChanged_Implementation(EGameStatus InStatus)
{
	ATestTaskAFokiHUD* HudPtr{ GetHUD<ATestTaskAFokiHUD>() };
	if (HudPtr)
	{
		HudPtr->SetGameStatus(InStatus);

		switch (GetNetMode())
		{
		case NM_Standalone:
			HudPtr->SetClientNetMode(EClientNetMode::Standalone);
			break;
		case NM_DedicatedServer:
		case NM_ListenServer:
			if (InStatus == EGameStatus::GameOver)
				GetWorld()->GetTimerManager().SetTimer(EndGameTimer, [this]() { EndSession(); }, 5.f, false);
			HudPtr->SetClientNetMode(EClientNetMode::Host);
			break;
		case NM_Client:
			HudPtr->SetClientNetMode(EClientNetMode::Client);
			break;
		default:
			break;
		}
	}

	switch (InStatus)
	{
	case EGameStatus::InLobby:
		SetShowMouseCursor(true);
		break;
	case EGameStatus::InGame:
		SetShowMouseCursor(false);
		break;
	case EGameStatus::GameOver:
		SetShowMouseCursor(true);
		break;
	}
}

void ATestTaskAFokiPlayerController::ClientGameComplete_Implementation(EGameResult InResult)
{
	ATestTaskAFokiHUD* HudPtr{ GetHUD<ATestTaskAFokiHUD>() };
	if (HudPtr == nullptr) return;

	HudPtr->SetGameResult(InResult);
	HudPtr->ShowGameInfo(true);
}

void ATestTaskAFokiPlayerController::ShowGameInfo()
{
	ATestTaskAFokiGameState* GS{ GetWorld()->GetGameState<ATestTaskAFokiGameState>() };
	if (GS == nullptr || GS->CurrentGameStatus() != EGameStatus::InGame) return;

	ATestTaskAFokiHUD* HudPtr{ GetHUD<ATestTaskAFokiHUD>() };
	if (HudPtr)
	{
		bShowedGameInfo = ~bShowedGameInfo;

		HudPtr->ShowGameInfo(bShowedGameInfo);
		SetShowMouseCursor(bShowedGameInfo);
	}
}

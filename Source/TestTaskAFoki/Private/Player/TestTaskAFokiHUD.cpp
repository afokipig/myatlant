// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/TestTaskAFokiHUD.h"
#include "Game/TestTaskAFokiGameMode.h"
#include "Types/GameStatus.h"


void ATestTaskAFokiHUD::SetGameStatus_Implementation(EGameStatus InStatus)
{
}

void ATestTaskAFokiHUD::SetGameResult_Implementation(EGameResult InResult)
{
}

void ATestTaskAFokiHUD::SetClientNetMode_Implementation(EClientNetMode InMode)
{
}

void ATestTaskAFokiHUD::GameStartClicked()
{
	if (ATestTaskAFokiGameMode * GM{ GetWorld()->GetAuthGameMode<ATestTaskAFokiGameMode>() })
		GM->StartGame();
}

void ATestTaskAFokiHUD::SetVictoryPart_Implementation(const FString& InPart)
{
}

void ATestTaskAFokiHUD::ExitButtonClicked_Implementation()
{
}

void ATestTaskAFokiHUD::ShowGameInfo_Implementation(bool bShow)
{
}

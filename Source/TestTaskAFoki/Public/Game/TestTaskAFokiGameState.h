// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "Types/GameStatus.h"
#include "TestTaskAFokiGameState.generated.h"


class AVictoryPart;
enum class EGameStatus : uint8;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnGameStatusChanged, EGameStatus, InStatus);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FVictoryPartTypeChanged, const FString&, InType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FVictoryPartsCountChanged, int32, InCurrent, int32, InToVictory);

DECLARE_DELEGATE(FOnVictoryPartDestroyed)

UCLASS()
class TESTTASKAFOKI_API ATestTaskAFokiGameState : public AGameState
{
	GENERATED_BODY()
	friend class ATestTaskAFokiGameMode;

public:
	virtual void Tick(float InDeltaTime) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	virtual void BeginPlay() override;

protected:
	void SetVictoryPartType(TSubclassOf<AVictoryPart> InType);
	void SetGameStatus(EGameStatus InGameStatus);
	void UpdateGamePartsInWorld(const TSet<TSubclassOf<AVictoryPart>> PartsTypes, bool bDeleteVictoryParts);

public:
	FORCEINLINE const UClass* const GetVictoryPartType() const { return VictoryPartType; }
	bool TryCollectVictoryPart(AVictoryPart* InPart, AController* InCollector);

	UFUNCTION(BlueprintPure, Category = "GameStatus")
	FORCEINLINE EGameStatus CurrentGameStatus() const { return GameStatus; }

	UFUNCTION(BlueprintPure, Category = "GameStatus")
	FORCEINLINE EGameResult GetGameResult() const { return GameResult; }

	UFUNCTION(BlueprintCallable, Category = "GameStatus")
	void BroadcastVictoryPartInfo();

	const FString& GetVictoryPartName() const;
	const FString& GetVictoryPartName(const UClass* const InType) const;

protected:
	UFUNCTION(NetMulticast, Reliable)
	void OnVictoryPartCollected(AVictoryPart* InPart, int32 InCurrentPartsCount);

	UFUNCTION(NetMulticast, Reliable)
	void OnVictoryPartTypeChanged(const FString& InTypeName);

protected:
	void StartGame();
	void EndGame(bool bVictory);
	UFUNCTION()
	void OnRep_GameStatus();

public:
	FOnGameStatusChanged OnGameStatusChanged;

	UPROPERTY(BlueprintAssignable, Category = "VictoryRules")
	FVictoryPartsCountChanged VictoryPartsCountChanged;

	UPROPERTY(BlueprintAssignable, Category = "VictoryRules")
	FVictoryPartTypeChanged VictoryPartTypeChanged;

	FOnVictoryPartDestroyed OnVictoryPartDestroyed;

private:
	UPROPERTY(Replicated, Transient)
	UClass* VictoryPartType{ nullptr };

	UPROPERTY(Replicated, Transient)
	int32 VictoryPartCollected{ 0 };
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VictoryRules", meta = (AllowPrivateAccess = "true"))
	int32 PartsCountToVictory{ 0 };
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VictoryRules", meta = (AllowPrivateAccess = "true"))
	float GameTime{ 300.f };

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "InGame", meta = (AllowPrivateAccess = "true"))
	float TimeToEndGame{ 0 };

	UPROPERTY(Transient)
	TArray<class ACollectablesSpawner*> Spawners;

	UPROPERTY(ReplicatedUsing = OnRep_GameStatus)
	EGameStatus GameStatus{ EGameStatus::InLobby };
	EGameResult GameResult{ EGameResult::None };

};

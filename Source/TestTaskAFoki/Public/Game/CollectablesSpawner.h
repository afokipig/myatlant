// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CollectablesSpawner.generated.h"

UCLASS()
class TESTTASKAFOKI_API ACollectablesSpawner : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ACollectablesSpawner();

	virtual void Tick(float InDeltaTime);

	void SpawnActorOfClass(UClass* InClass);
	FORCEINLINE AActor* GetSpawnedObject() { return SpawnedObject; }
	FORCEINLINE bool CanSpawn() const { return SpawnedObject == nullptr; }

private:
	UFUNCTION()
	void OnActorDestroyed(AActor* InOther);

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawner", meta = (AllowPrivateAccess = "true"))
	class UArrowComponent* SpawnArrow{ nullptr };

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawner", meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* SpawnerMesh{ nullptr };

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawner", meta = (AllowPrivateAccess = "true"))
	float SpawnerMeshRotationSpeed{ 5.f };

	UPROPERTY()
	AActor* SpawnedObject{ nullptr };
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactive/InteractiveActor.h"
#include "TeleportBase.generated.h"

/**
 * 
 */
UCLASS()
class TESTTASKAFOKI_API ATeleportBase : public AInteractiveActor
{
	GENERATED_BODY()
public:
	ATeleportBase();

public:
	virtual bool InteractionPossible() const override;
	virtual bool TryInteract(AActor* InInteractor) override;
	
private:
	UFUNCTION(NetMulticast, Reliable)
	void Teleport(AActor* InActor, const FVector& Position);

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Pair", meta = (AllowPrivateAccess = "true"))
	ATeleportBase* Pair{ nullptr };
};

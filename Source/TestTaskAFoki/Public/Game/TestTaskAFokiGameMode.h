// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "Types/GameStatus.h"
#include "TestTaskAFokiGameMode.generated.h"


UCLASS(minimalapi)
class ATestTaskAFokiGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ATestTaskAFokiGameMode();
	virtual void PreInitializeComponents() override;
	virtual void RestartPlayer(AController* NewPlayer) override;
	void StartGame();

protected:
	virtual void OnPostLogin(AController* NewPlayer) override;
	void OnVictoryPartDestroyed();

private:
	TSubclassOf<class AVictoryPart> GetRandomVictoryPartType() const;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "GameStatus", meta = (AllowPrivateAccess = "true"))
	EGameStatus GameStatus{ EGameStatus::InLobby };

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VictoryRules", meta = (AllowPrivateAccess = "true"))
	TSet<TSubclassOf<class AVictoryPart>> PossibleVictoryParts;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VictoryRules", meta = (AllowPrivateAccess = "true"))
	int32 StartVictoryPartBatches{ 1 };
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "TestTaskAFokiHUD.generated.h"


enum class EGameStatus : uint8;

UENUM(BlueprintType)
enum class EClientNetMode : uint8
{
	Standalone UMETA(DisplayName = "Standalone"),
	Host UMETA(DisplayName = "Host"),
	Client UMETA(DisplayName = "Client"),
};

UCLASS()
class TESTTASKAFOKI_API ATestTaskAFokiHUD : public AHUD
{
	GENERATED_BODY()
public:

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GameStatus")
	void SetGameStatus(EGameStatus InStatus);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GameStatus")
	void SetGameResult(EGameResult InResult);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GameStatus")
	void SetClientNetMode(EClientNetMode InMode);

	UFUNCTION(BlueprintCallable, Category = "GameStatus")
	void GameStartClicked();

	UFUNCTION(BlueprintNativeEvent, Category = "GameStatus")
	void SetVictoryPart(const FString& InPart);

	UFUNCTION(BlueprintNativeEvent, Category = "GameStatus")
	void ExitButtonClicked();

	UFUNCTION(BlueprintNativeEvent, Category = "GameStatus")
	void ShowGameInfo(bool bShow);
};

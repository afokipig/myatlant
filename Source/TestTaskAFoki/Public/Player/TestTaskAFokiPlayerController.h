// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TestTaskAFokiPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class TESTTASKAFOKI_API ATestTaskAFokiPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	void OnPostLogin();

protected:
	virtual void EndPlay(EEndPlayReason::Type InReason) override;
	virtual void SetupInputComponent() override;;
	
	void EndSession();

public:
	UFUNCTION()
	void OnGameStatusChanged(EGameStatus InStatus);

	UFUNCTION(BlueprintCallable, Category = "HUD")
	void OnHUDCreated();

private:
	UFUNCTION(Client, Reliable)
	void ClientGameStatusChanged(EGameStatus InStatus);

	UFUNCTION(Client, Reliable)
	void ClientGameComplete(EGameResult InResult);

	void ShowGameInfo();

private:
	FTimerHandle EndGameTimer;

	UPROPERTY(EditAnywhere, Category = "Input", meta = (AllowPrivateAccess = "true"))
	class UInputAction* ShowGameInfoAction{ nullptr };

	uint8 bShowedGameInfo : 1{false};
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/WidgetComponent.h"
#include "TargetTrackedWidgetComponent.generated.h"

/**
 * 
 */
UCLASS()
class TESTTASKAFOKI_API UTargetTrackedWidgetComponent : public UWidgetComponent
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;

public:
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void SetTarget(AActor* InTarget);

	UFUNCTION()
	void PopTarget(AActor* InTarget);

private:
	AActor* Target{ nullptr };
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


UENUM(BlueprintType)
enum class EGameStatus : uint8
{
	InLobby UMETA(DisplayName = InLobby),
	InGame UMETA(DisplayName = InGame),
	GameOver UMETA(DisplayName = GameOver)
};

UENUM(BlueprintType)
enum class EGameResult : uint8
{
	None UMETA(Hidden),
	Win UMETA(DisplayName = Win),
	Lose UMETA(DisplayName = Lose),
};
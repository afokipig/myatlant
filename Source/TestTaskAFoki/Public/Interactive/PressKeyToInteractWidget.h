// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PressKeyToInteractWidget.generated.h"

/**
 *
 */
UCLASS()
class TESTTASKAFOKI_API UPressKeyToInteractWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UFUNCTION()
	void SetKeyString(const FString& InKey);

private:
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* KeyText{ nullptr };
};

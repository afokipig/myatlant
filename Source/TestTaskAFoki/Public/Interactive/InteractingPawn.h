// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "InteractingPawn.generated.h"


class AInteractiveActor;

UINTERFACE(MinimalAPI)
class  UInteractingPawn : public UInterface
{
	GENERATED_BODY()
};

class IInteractingPawn
{
	GENERATED_BODY()
public:

	virtual FString GetInteractionKey() const;
	virtual void PushInteractiveActor(AInteractiveActor* InActor);
	virtual void PopInteractiveActor(AInteractiveActor* InActor);

	virtual bool NeedWidget() const;
};
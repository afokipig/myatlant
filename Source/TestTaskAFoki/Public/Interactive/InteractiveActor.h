// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractiveActor.generated.h"


class UShapeComponent;

UCLASS()
class TESTTASKAFOKI_API AInteractiveActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInteractiveActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual bool InteractionPossible() const;
	virtual bool TryInteract(AActor* InInteractor);

protected:
	UFUNCTION(BlueprintNativeEvent, Category = "Interaction")
	UShapeComponent* GetInteractionShape() const;

	UFUNCTION()
	void OnInteractionShapeBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void OnInteractionShapeEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	void AddInteractor(AActor* InActor);
	void RemoveInteractor(AActor* InActor);
	void RemoveAllInteractors();

private:
	void BindCollision(UShapeComponent* InShapeComponent);

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Component", meta = (AllowPrivateAccess = "true"))
	class UTargetTrackedWidgetComponent* WidgetComponent;
};

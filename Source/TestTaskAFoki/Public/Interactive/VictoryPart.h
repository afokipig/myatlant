// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactive/InteractiveActor.h"
#include "VictoryPart.generated.h"

/**
 * 
 */
UCLASS()
class TESTTASKAFOKI_API AVictoryPart : public AInteractiveActor
{
	GENERATED_BODY()
public:
	AVictoryPart();

	virtual bool InteractionPossible() const override;
	virtual bool TryInteract(AActor* InInteractor) override;

	const FString& GetPartName() const { return PartName; }

	void SetCollected() { if(HasAuthority()) bIsValidPart = false; }
	FORCEINLINE bool IsValidPart() const { return bIsValidPart; }

	virtual void Destroyed() override;

private:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Component", meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* DoorMesh{ nullptr };

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Description", meta = (AllowPrivateAccess = "true"))
	FString PartName;

	uint8 bIsValidPart : 1{true};
};

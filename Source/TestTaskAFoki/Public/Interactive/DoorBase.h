// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactive/InteractiveActor.h"
#include "DoorBase.generated.h"


UENUM()
enum class EDoorStatus : uint8
{
	Open,
	Move,
	Closed
};

/**
 *
 */
UCLASS()
class TESTTASKAFOKI_API ADoorBase : public AInteractiveActor
{
	GENERATED_BODY()
public:
	ADoorBase();

protected:
	virtual void BeginPlay() override;

public:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual void Tick(float DeltaTime) override;
	virtual bool InteractionPossible() const override;
	virtual bool TryInteract(AActor* InInteractor) override;

protected:
	UFUNCTION()
	void OnRep_TargetDoorStatus();

	void SetTargetDoorStatus(EDoorStatus InStatus);

private:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Component", meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* DoorMesh{ nullptr };

	float TargetDoorState{ 0.f };

	float CurrentState{ -100.f };

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "States", meta = (AllowPrivateAccess = "true"))
	float CloseState{ 0.f };
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "States", meta = (AllowPrivateAccess = "true"))
	float OpenState{ -100.f };

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement", meta = (AllowPrivateAccess = "true"))
	float DoorMoveSpeed{ 1.f };

	FVector BeginRelativeLocation;

	UPROPERTY(ReplicatedUsing = OnRep_TargetDoorStatus)
	EDoorStatus TargetDoorStatus{ EDoorStatus::Closed };
	EDoorStatus CurrentDoorStatus : 2{EDoorStatus::Closed};
};
